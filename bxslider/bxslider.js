/**
 * Created by fwlst on 2014/10/28.
 */

$(function(){
    $(".main").bxSlider();
});



(function($){
    $.fn.bxSlider = function(){
        return this.each(function(){
            var $this = $(this);
            var $ctrl = $this.find(".ctrl li"),
                $slider = $this.find(".slider li"),
                $title = $this.find(".slider div"),
                $next = $this.find(".arrow .next"),
                $prev = $this.find(".arrow .prev");

            $ctrl.click(function(){
                var index = 0
                for(var i = 0; i < $ctrl.length; i++){
                    if($ctrl[i] === this){
                        index = i;
                        break
                    };
                };
                $title.hide()
                $($title[index]).show();
                $slider.hide();
                $($slider[index]).fadeIn();
                $ctrl.css({
                    background:"yellow"
                });
                $($ctrl[index]).css({
                    background:"#666"
                });
            });


            var index = 0;

            function location(){
                for(var i = 0; i < $title.length; i++){
                    if($($title[i]).css("display") === "block"){
                        index = i;
                        break
                    };
                };
            };


            $next.click(function(){
                location();
                if(index === $title.length - 1){
                    index = -1;
                };
                $title.hide()
                $($title[index + 1]).show();
                $slider.hide();
                $($slider[index +1]).fadeIn();
                $ctrl.css({
                    background:"yellow"
                });
                $($ctrl[index +1]).css({
                    background:"#666"
                });

            });
            $prev.click(function(){
                location();
                if(index === 0){
                    index = $title.length;
                };
                $title.hide()
                $($title[index -1]).show();
                $slider.hide();
                $($slider[index -1]).fadeIn();
                $ctrl.css({
                    background:"yellow"
                });
                $($ctrl[index -1]).css({
                    background:"#666"
                });

            });



        });

    };
})(jQuery)