/**
 * Created by fwlst on 2014/10/30.
 */




$(function(){
    $(".main").standard();
});




(function($){
    $.fn.standard = function(){
        return this.each(function(){
            var $this = $(this);
            var $ctrl = $this.find(".ctrl li"),
                $slider = $this.find(".slider ul"),
                $title = $this.find(".slider div"),
                $next = $this.find(".arrow .next"),
                $prev = $this.find(".arrow .prev"),
                $slider_length = $this.find(".slider img").length,
                $page = 1,
                $page_width = $(".main").width();

            var index = 0;

            $next.click(function(){
                if(!$slider.is(":animated")){
                    if ($page == $slider_length) {
                        $slider.animate({left: 0 + "px"});
                        $page = 1, index = 0;
                    } else {
                        $slider.animate({left: "-=" + $page_width + "px"});
                        $page++, index++;
                    };
                    $title.hide();
                    $($title[index]).show();
                    $ctrl.css({
                        background:"yellow"
                    });
                    $($ctrl[index]).css({
                        background:"#666"
                    });
                };
            });

            $ctrl.click(function(){
                var index = 0
                for(var i = 0; i < $ctrl.length; i++){
                    if($ctrl[i] === this){
                        index = i;
                        break
                    };
                };
                $slider.animate({left:"-"+ $page_width*index + "px"});

                $title.hide();
                $($title[index]).show();
                $ctrl.css({
                    background:"yellow"
                });
                $($ctrl[index]).css({
                    background:"#666"
                });


            });

            $prev.click(function(){
                if(!$slider.is(":animated")){
                    if($page == 1){
                        $slider.animate({left:"-"+$page_width*($slider_length-1)+"px"});
                        $page = $slider_length, index = $slider_length-1;
                    }else{
                        $slider.animate({left:"+="+$page_width+"px"});
                        $page--, index--;
                    };
                    $title.hide();
                    $($title[index]).show();
                    $ctrl.css({
                        background:"yellow"
                    });
                    $($ctrl[index]).css({
                        background:"#666"
                    });
                };
            });

        });

    };
})(jQuery);